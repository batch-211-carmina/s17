// printName(); - can invoked thru hoisting
// showSampleAlert(); - it will work

console.log("Hello World!");


//FUNCTIONS
	//Functions in JavaScript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.

	//Functions are mostly created to create complicated task to run several lines of code in succession.

	//They are also used to prevent repeating lines/blocks of code that perform the same task/function.


	//Function Declarations
		//(function statement) defines a funcion with the specified parameters
		/*
			Syntax:

			function functionName() {
	
			}
		*/

		//function keyword - used to define a JavaScript function
		//functionName - the function name. Functions are named to be able to use later in the code.
		//function block {} - the statement which comprise the body of the function. This is where the code is to be executed.
		//we can assign a variable to hold a function, but that will be explained later

		function printName() {
			console.log("My name is John.");
		};

		printName();

		//semicolons are used to separate executable JavaScript statements.

		//Function Invocation
			//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called
			//It is common to use the term "call a function" instead of "invoke a function"
		
		printName();

			// declaredFunction(); //error much like variables, we cannot invoke a function we have yet to define.
	

	//Function Declarations vs Expressions

		//Function Declarations
			//a function can be create through function declaration by using the function keyword and adding a function name.
			//declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked call upon

			declaredFunction(); //declared functions can be hoisted, as long as the function has been defined

			function declaredFunction() {
				console.log("Hello World from declaredFunction()");
			}

		//NOTE: Hoisting is JS's behavior for certain variables and functions to run or use them before their declaration

			declaredFunction() //you can use the function or invoke again and again


		//Functions Expression
			// a function can also be stored in a variable. This is called a "function expression".

			// a function expression is an anonymous function assigned to the variableFunction

			// Anonymous function - is a function without a name

				// variableFunction(); //error
					/*
						will give an error - function expression, being stored in a let or const variable cannot be hoisted
					*/

			let variableFunction = function() {
				console.log("Hello Again");
			}

			variableFunction();


			//We can also create a function expression of a named function
			//However, to invoke the function expression, we invoke it by its variable name, not its function name
			//Function expressions are always invoked (called) using the varible name

			let funcExpression = function funcName() {
				console.log("Hello From The Other Side");
			}

			// funcName();  //error
			funcExpression();


			// you can reassign declared functions and function expressions to new anonymous functions

			declaredFunction = function() {
				console.log("updated declaredFunction");
			}

			declaredFunction();

			funcExpression = function() {
				console.log("updated funcExpression");
			}

			funcExpression();

			const constantFunc = function() {
				console.log("Initialize with const");
			}

			constantFunc();

			// However, we cannot reassign a function expression initialized with const.

			/*constantFunc = function() {
				console.log("Cannot be re-assigned");
			}
			constantFunc();* //error


		//Function Scoping

		/*
			Scope is the accesibility (visibility) of variables within our program JS variables has 3 types of scope:
			1. local/block scope
			2. global scope
			3. function scope
		*/

			// let globalVar = "Mr. Worldwide";  //it will work inside the local/block scope when invoke
			// console.log(globalVar);

			{
				let localVar = "Armando Perez";
				console.log(localVar);
				// console.log(globalVar);  //return an error
				// we cannot invoke a global var inside a block if it is not declared before our code block
			}

			let globalVar = "Mr. Worldwide";
			console.log(globalVar);
			// console.log(localVar); //return an error
			//localVar being in a block, cannot be accessed outside of its code block

			//Function Scope
			/*
				JS has a function scope: Each function creates a new scope
				Variables defined inside a function are not accessible (visible) from outside the function
				Variables declared with var, let, and const are quite similar when declared inside a fuction
			*/

			function showNames() {

				//Functions scoped variables
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet = "Jane";

				console.log(functionVar);
				console.log(functionConst);
				console.log(functionLet);
			};

			showNames();

				// console.log(functionVar);  //return an error
				// console.log(functionConst); //return an error
				// console.log(functionLet); //return an error

			/*
				the variables functionVar, functionConst, and functionLet are functions scoped and cannot be accessed outside of the function they were declared in
			*/


		//Nested Functions

			//You can create another function inside a function
			//This is called a nested function


			function myNewFunction() {
				let name = "Cee";

				function nestedFunction() {
					let nestedName = "Thor";
					console.log(name);
					console.log(nestedName);
				}
				// console.log(nestedName); 
				//results to an error
				//nestedName is not defined
				//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in

				nestedFunction();
			};

			myNewFunction();
			// nestedFunction();
			/*
				since the function is declared inside myNewFunction, it also cannot be invoked outside of the function it was declared in
			*/


		//Function and Global Scoped Variables

			//Global Scoped Varible
			let globalName = "Nej";

			function myNewFunction2() {
				let nameInside = "Martin";

				//Variables declared globally (outside any function) have Global sope
				//Global variables can be access from anywhere in a JS program including from inside a function
				console.log(globalName);
			}

			myNewFunction2();

	//Using alert() method

		//alert() allows us to show a small window at the top of our browser page to show information to our users
		//as opposed to a console.log() which will only show the message on the console
		//it allows us to show a short dialog or instruction to our user
		//the page will wait until the user dismisses the diaglog

		alert("Hello World"); //This will run immediately when the page loads

		//Syntax: alert()
		//alert("messageString");
		//you can do it numbers too

		//You can also use an alert() to show a message to the user from a later function invocation

		function showSampleAlert() {
			alert("Hello, User");
		};

		showSampleAlert();

		//you will find that the page waits for the user to dismiss the dialog before proceeding
		//you can witness this by reloading the page while the console is open
		console.log("I will only log in the console when the alert is dismissed.");

	//NOTES on using of alert()
		//Show only an alert() for short dialogs/messages to the user
		//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing


	//Using prompt()

		//prompt() allows us to show a small window at the top of our browser to gather user input
		//It much like alert(), will have the page wait until the user completes or enters their input
		//The input from the prompt() will be returned as a String once the user dismisses the window

		let samplePrompt = prompt("Enter your Name");
		console.log("Hello ," + samplePrompt);

		/*
			Syntax: prompt()

			prompt("dialogInString");
		*/

		let sampleNullPrompt = prompt("Don't enter anything");
		console.log(sampleNullPrompt);
		
		//prompt() returns an empty string when there is no input
		//null if the user cancels the prompt()

		//prompt() can be used for us to gather user input and be used in our code
		//However, since prompt() windows will have the page wait until th user dismisses the window it must not be overused

		//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function

		function printWelcomeMessage() {
			let firstName = prompt("Enter Your First Name");
			let lastName = prompt("Enter Your Last Name");
			console.log("Hello " + firstName+ " " + lastName + "!");
		}

		printWelcomeMessage();


	//Function Naming Conventions

		//functions names should be definitive of the task it will perform. It usually contains a verb

		function getCourses() {
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}

		getCourses();

		//AVOID generic names to avoid confusion within your code
		/*function get() {
			let name = "Jamie";
			console.log(name);
		}

		get();*/ //not a best practice


		//AVOID pointless and inappropriate function names
		/*function pikachu() {
			console.log(25 % 5);
		};
		pikachu();*/  //not a best practice


		//Name your functions in small caps. Follow camelCase when naming variables and functions

		function displayCarInfo() {
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");

		}
		displayCarInfo();

