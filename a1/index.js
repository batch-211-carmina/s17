/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	function myInfo() {
		let myName = prompt("What is your Name?");
		let myAge = prompt("How old are you?");
		let myAddress = prompt("Where do you live?");
		console.log("Hello, " + myName);
		console.log("You are " + myAge + " years old.");
		console.log("You live in " + myAddress);
	}

	myInfo();


	let myFavoriteBand = function(){
		console.log("1. Backtreet Boys");
		console.log("2. A1");
		console.log("3. Boyz II Men");
		console.log("4. The Beatles");
		console.log("5. Eraserheads");
	}

	myFavoriteBand();
	

	let movie1 = "The Godfather"
	let movie2 = "Titanic"
	let movie3 = "Vampire Diaries"
	let movie4 = "Strangers Thing"
	let movie5 = "Psycho"

	function myFavoriteMovies() {
		console.log("1. " + movie1)
		console.log("Rotten Tomatoes Rating: 97%")
		console.log("2. " + movie2)
		console.log("Rotten Tomatoes Rating: 96%")
		console.log("3. " + movie3)
		console.log("Rotten Tomatoes Rating: 91%")
		console.log("4. " + movie4)
		console.log("Rotten Tomatoes Rating: 93%")
		console.log("5 ." + movie5)
		console.log("Rotten Tomatoes Rating: 95%")
	}

	myFavoriteMovies();


	// printUsers();
	let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};	
	printFriends();


// console.log(friend1);
// console.log(friend2);